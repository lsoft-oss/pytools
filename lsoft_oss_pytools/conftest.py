#!/usr/bin/python
# -*- coding: utf-8 -*-

import logging
import os

LOG = logging.getLogger(__name__)

import pytest


@pytest.fixture(scope='session')
def rootdir(pytestconfig):
    oldcwd = os.getcwd()
    rootdir = str(pytestconfig.rootdir)
    os.chdir(rootdir)
    yield rootdir
    os.chdir(oldcwd)
