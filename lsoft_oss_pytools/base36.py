#!/usr/bin/env python
# -*- coding: utf-8 -*-

__author__ = 'Marco Bartel'

import datetime
import time
import uuid
from textwrap import wrap


def hexchecksum(hexStr):
    ret = 0
    for h in hexStr:
        ret += int(h, 16)
    return base36encode(ret % 36)


def hexchecksumSlicer(hexStr, slice):
    ret = ""
    for h in wrap(hexStr, slice):
        ret += hexchecksum(h)
    return ret


def node36():
    return hexchecksumSlicer("{:x}".format(uuid.getnode()), 4)


def base36encode(number):
    """
    Encodes a int number into a base36 string
    :param number: int
    :return: string
    """
    if not isinstance(number, int):
        raise TypeError('number must be an integer')
    if number < 0:
        raise ValueError('number must be positive')

    alphabet, base36 = ['0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ', '']

    while number:
        number, i = divmod(number, 36)
        base36 = alphabet[i] + base36

    return base36 or alphabet[0]


def base36decode(base36):
    """
    Decodes a base36 string into an int number
    :param base36: string
    :return: int
    """
    alphabet = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    ret = 0
    for c in base36:
        ret *= 36
        ret += alphabet.index(c)
    return ret


def base36uuid(random=5):
    systime = time.time()
    sdt = datetime.datetime(year=2010, month=1, day=1)
    dt = datetime.datetime.fromtimestamp(systime)

    random36 = base36encode(uuid.uuid4().int)[:random]
    node36val = node36()
    onlytime36 = base36encode(dt.hour * 3600 + dt.minute * 60 + dt.second)
    onlydate36 = base36encode((dt - sdt).days)

    hash = "{mdate}-{mtime}-{mnode}-{mrand}".format(
        mrand=random36,
        mdate=onlydate36,
        mtime=onlytime36,
        mnode=node36val
    )
    return hash


def base36uuid_decode(hash):
    sdt = datetime.datetime(year=2010, month=1, day=1)
    onlydate36, onlytime36, node36, random36 = hash.split("-")

    onlydate = base36decode(onlydate36)
    onlytime = base36decode(onlytime36)
    dt = sdt + datetime.timedelta(days=onlydate, seconds=onlytime)

    return {
        "timestamp": dt,
        "node": node36,
        "random": random36
    }


__all__ = [base36uuid, base36uuid_decode, base36encode, base36decode]
