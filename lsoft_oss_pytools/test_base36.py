#!/usr/bin/python
# -*- coding: utf-8 -*-

import logging

import pytest

from lsoft_oss_pytools.base36 import hexchecksum, hexchecksumSlicer, base36encode, base36decode, base36uuid, node36, base36uuid_decode

LOG = logging.getLogger(__name__)


def test_hexchecksum():
    assert hexchecksum("0123456789abcdef") == 'C'


def test_hexchecksum_slicer():
    assert hexchecksumSlicer("0123456789abcdef", 4) == '6M2I'


def test_node36():
    r1 = node36()
    r2 = node36()
    assert r1 == r2
    assert len(r1) <= 4


def test_base36encode():
    assert base36encode(12345678) == '7CLZI'
    with pytest.raises(TypeError):
        base36encode('231')
    with pytest.raises(ValueError):
        base36encode(-213)


def test_base36decode():
    assert base36decode('7CLZI') == 12345678


def test_base36uuid():
    assert base36uuid_decode(base36uuid())['node'] == node36()


def test_base36uuid_decode():
    assert base36uuid_decode(base36uuid())['node'] == node36()
